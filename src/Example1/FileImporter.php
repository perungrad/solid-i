<?php

namespace Workshop\Solid\Example1;

use Workshop\Solid\Example1\FileInterface;

class FileImporter
{
    /**
     * @param FileInterface $file
     */
    public function importFile(FileInterface $file)
    {
        $location = '@TODO';

        $file->rename($location);
    }
}

