<?php

namespace Workshop\Solid\Example1;

class PermissionManager
{
    /** @var string */
    private $user;

    /** @var string */
    private $group;

    /**
     * @param string $user
     * @param string $group
     */
    public function __construct($user, $group)
    {
        $this->user = $user;
        $this->group = $group;
    }

    /**
     * @param FileInterface $file
     */
    public function transferOwner(FileInterface $file)
    {
        $file->changeOwner($this>user, $this->group);
    }
}

