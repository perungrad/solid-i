<?php

namespace Workshop\Solid\Example3\Project;

use Workshop\Solid\Example3\ORM\EntityManager;

class UserRepository
{
    public function __construct(EntityManager $entityManager) {
        // ...
    }
}

