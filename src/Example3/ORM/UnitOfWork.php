<?php

namespace Workshop\Solid\Example3\ORM;

use Workshop\Solid\Example3\ORM\EntityInterface;

class UnitOfWork
{
    /**
     * @param EntityInterface $entity
     *
     * @return bool
     */
    public function doSomethingDangerous(EntityInterface $entity)
    {
        return true;
    }

    /**
     * @return bool
     */
    public function writeToDb()
    {
        return true;
    }
}

