<?php

namespace Workshop\Solid\Example3\ORM;

use Workshop\Solid\Example3\ORM\UnitOfWork;
use Workshop\Solid\Example3\ORM\EntityInterface;

class EntityManager
{
    /** @var UnitOfWork */
    private $unitOfWork;

    /** @var array */
    private $entities = [];

    public function __construct()
    {
        $this->unitOfWork = new UnitOfWork();
    }

    /**
     * @param EntityInterface $entity
     *
     * @return self
     */
    public function persist(EntityInterface $entity)
    {
        $this->entities[] = $entity;

        return $this;
    }

    /**
     * @return bool
     */
    public function flush()
    {
        foreach ($this->entities as $entity) {
            $this->getUnitOfWork()->doSomethingDangerous($entity);
        }

        $this->unitOfWork->writeToDb();
    }

    private function getUnitOfWork()
    {
        return $this->getUnitOfWork;
    }
}

