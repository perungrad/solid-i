<?php

namespace Workshop\Solid\Example3\ORM;

use Workshop\Solid\Example3\ORM\EntityManager;

class OrmServiceProvider
{
    /**
     * @param ServiceContainerInterface $serviceContainer
     */
    public function defineServices(ServiceContainerInterface $serviceContainer)
    {
        $serviceContainer->set('entity_manager', function () {
            return new EntityManager();
        });
    }
}

