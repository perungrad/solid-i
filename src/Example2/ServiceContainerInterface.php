<?php

namespace Workshop\Solid\Example2;

class ServiceContainerInterface
{
    /**
     * @param string $name
     *
     * @return mixed
     */
    public function get($name);

    /**
     * @param string   $name
     * @param callable $factory
     *
     * @return self
     */
    public function set($name, callable $factory);
}

