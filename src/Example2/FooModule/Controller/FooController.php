<?php

namespace Workshop\Solid\Example2\FooModule\Controller;

use Workshop\Solid\Example2\ServiceContainerInterface;

class FooController
{
    /**
     * @param ServiceContainerInterface $serviceContainer
     */
    public function detailAction(ServiceContainerInterface $serviceContainer)
    {
        $repository = $serviceContainer->get('repository.user');
        $templating = $serviceContainer->get('templating');

        // do something useful
    }
}

