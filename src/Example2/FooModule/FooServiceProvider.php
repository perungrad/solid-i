<?php

namespace Workshop\Solid\Example2\FooModule;

use Workshop\Solid\Example2\ServiceContainerInterface;

class FooServiceProvider
{
    /**
     * @param ServiceContainerInterface $serviceContainer
     */
    public function defineServices(ServiceContainerInterface $serviceContainer)
    {
        $serviceContainer->set('mailer', function () use ($serviceContainer) {
            return new Mailer(
                $serviceContainer->get('mailer.transport')
            );
        });

        $serviceContainer->set('mailer.transport', function () use ($serviceContainer) {
            return new MailerSmtpTransport();
        });
    }
}

