<?php

namespace Workshop\Solid\Example2;

class ServiceContainer implements ServiceContainerInterface
{
    private $factories = [];

    /**
     * @param string $name
     *
     * @return mixed
     */
    public function get($name)
    {
        if (array_key_exists($name, $this->factories)) {
            $factory = $this->factories[$name];

            return $factory();
        }

        return null;
    }

    /**
     * @param string   $name
     * @param callable $factory
     *
     * @return self
     */
    public function set($name, callable $factory)
    {
        $this->factories[$name] = $factory;

        return $this;
    }
}

